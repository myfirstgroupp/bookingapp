import { CanActivateFn, CanMatchFn, Router } from '@angular/router';
import { AuthenticationResponse } from '../models/authentication-response';
import { JwtHelperService } from '@auth0/angular-jwt';
import { of } from 'rxjs';
import { inject } from '@angular/core';

export const authGuard: CanMatchFn = (route, state) => {
  const router = inject(Router);
  const storedUser = localStorage.getItem('user');
  if (storedUser) {
    const authResponse: AuthenticationResponse = JSON.parse(storedUser);
    //TOKEN EXTRACTION FROM API RESPONSE BACKEND
    const token = authResponse.jwt;
    if (token) {
      const jwtHelper = new JwtHelperService();
      const isTokenNonExpired = !jwtHelper.isTokenExpired(token);
      if (isTokenNonExpired) {
        return of(true);
      }
    }
  }
  router.navigate(['signin']);
  //router.createUrlTree(['signin']);
  return of(false);
};
