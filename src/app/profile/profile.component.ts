import { Component, Input, LOCALE_ID, OnInit } from '@angular/core';
import { CustomerDTO } from '../models/customer-dto';
import { AuthService } from '../services/authentication/authentication.service';
import { AuthenticationResponse } from '../models/authentication-response';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CreateBookingComponent } from '../components/bookings/create-booking/create-booking.component';

import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { BookingCreateRequest } from '../models/booking-create-request';
import { ReservaService } from '../services/reserva.service';


@Component({
  selector: 'app-customer-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  bookings: BookingCreateRequest[] = [];
  dataSource!: MatTableDataSource<BookingCreateRequest>;

  //crear instancias
  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
    private reservaService: ReservaService
  ) { }

  displayedColumns: string[] = [
    'position',
    'dateReserva',
    'tipoReserva',
    'hourStart',
    'hourFinish',
    'quantityPersons',
    'observations',
    'statusReserva',
  ];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  ngOnInit(): void {
    const userId = this.userId;
       this.reservaService
         .getReservas(userId)
         .subscribe((response: BookingCreateRequest[]) => {
           this.bookings = response;
           this.dataSource = new MatTableDataSource<BookingCreateRequest>(
             this.bookings
           );
           this.dataSource.paginator = this.paginator;
           console.log(response);
         });
  }

  ngAfterViewInit() {
  }

  //dialog
  openDialog() {
    //configurar comportamientos del dialog
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true; // no podra cerrar el dialog con clic fuera de el
    dialogConfig.autoFocus = true;

    this.dialog.open(CreateBookingComponent, dialogConfig);
  }

  private getAuthResponse(): AuthenticationResponse | null {
    const storedUser = localStorage.getItem('user');
    if (storedUser) {
      return JSON.parse(storedUser);
    }
    return null;
  }

  get tipoDoc(): string {
    const authResponse = this.getAuthResponse();
    if (authResponse && authResponse.data && authResponse.data.tipoDocument) {
      return authResponse.data.tipoDocument;
    }
    return '--';
  }

  get document(): string {
    const authResponse = this.getAuthResponse();
    if (authResponse && authResponse.data && authResponse.data.document) {
      return authResponse.data.document;
    }
    return '--';
  }

  get firstName(): string {
    const authResponse = this.getAuthResponse();
    if (authResponse && authResponse.data && authResponse.data.firstName) {
      return authResponse.data.firstName;
    }
    return '--';
  }

  get lastName(): string {
    const authResponse = this.getAuthResponse();
    if (authResponse && authResponse.data && authResponse.data.lastName) {
      return authResponse.data.lastName;
    }
    return '--';
  }

  get email(): string {
    const authResponse = this.getAuthResponse();
    if (authResponse && authResponse.data && authResponse.data.email) {
      return authResponse.data.email;
    }
    return '--';
  }

  get userId(): string {
    const authResponse = this.getAuthResponse();
    if (authResponse && authResponse.data && authResponse.data.customerId) {
      return authResponse.data.customerId;
    }
    return '--';
  }
}
