export class Utils {
  validateKey(evt: any): boolean {
    const key = String.fromCharCode(evt.keyCode);
    const regex = /^[0-9]+$/; // Regex para solo numeros
    return regex.test(key);
  }

  validateKeyText(evt: any): boolean {
    const key = String.fromCharCode(evt.keyCode);
    const regex = /^[A-Za-z\b]+$/; // Regex para letras del alfabeto y backspace (\b)
    return regex.test(key);
  }
}
