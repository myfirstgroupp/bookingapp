import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MessageService } from 'primeng/api';
import { CustomerService } from '../services/customer.service';
import { Utils } from '../utils/Utils';

  interface tipoDoc {
    value: string;
    viewValue: string;
  }

  /** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [MessageService],
})
export class SignupComponent {
  utils = new Utils();

  typesDocs: tipoDoc[] = [
    { value: 'CC', viewValue: 'CC' },
    { value: 'VISA', viewValue: 'Visa' },
    { value: 'PASSPORT', viewValue: 'Passport' },
  ];

  formSignup: FormGroup;
  tipoDocument!: FormControl;
  document!: FormControl;
  firstName!: FormControl;
  lastName!: FormControl;
  phone!: FormControl;
  birthdate!: FormControl;
  email!: FormControl;
  password!: FormControl;

  constructor(
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private customerService: CustomerService
  ) {
    this.formSignup = this.formBuilder.group({
      tipoDocument: new FormControl('', [Validators.required]),
      document: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[0-9]\d{6,10}$/),
      ]),
      firstName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      lastName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      phone: new FormControl('', [Validators.required]),
      birthdate: new FormControl(),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        ///[A-Za-z\d]  cualquier A-Z a-z and numbers
        Validators.pattern(/.*\d.*/),
      ]),
    });
  }

  save() {
    const dataForm = this.formSignup.value;
    this.customerService.registerCustomer(dataForm).subscribe({
      next: () => {
        this.messageService.add({
          severity: 'success',
          summary: 'Success',
          detail: `The customer ${dataForm.firstName} was created successfully.`,
        });
      },
      error: (err) => {
        if (err.error.message === 'Error: Document is already in use!') {
          this.messageService.add({
            severity: 'warn',
            summary: 'Document already exists',
            detail: `The document ${dataForm.document} already exists`,
          });
        }
        if (err.error.message === 'Error: Email is already in use!') {
          this.messageService.add({
            severity: 'warn',
            summary: 'Email already exists',
            detail: `The email ${dataForm.email} already exists`,
          });
        }

        if (err.error.statusCode === 500) {
          this.messageService.add({
            severity: 'error',
            summary: 'Server Error',
            detail: `Error internal`,
          });
        }
      },
    });
  }

  get getEmail() {
    return this.formSignup.get('email')!;
  }

  onKeyPressNumber(event: any) {
    if (!this.utils.validateKey(event)) {
      event.preventDefault();
    }
  }

  onKeyPressText(event: any) {
    if (!this.utils.validateKeyText(event)) {
      event.preventDefault();
    }
  }

  get getDocument() {
    return this.formSignup.get('document')!;
  }

  get getFirstName() {
    return this.formSignup.get('firstName')!;
  }

  get getLastName() {
    return this.formSignup.get('lastName')!;
  }

  get getPass() {
    return this.formSignup.get('password')!;
  }

  matcher = new MyErrorStateMatcher();
}
