import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MessageService } from 'primeng/api';
import { AuthenticationResponse } from 'src/app/models/authentication-response';
import { ReservaService } from 'src/app/services/reserva.service';

interface tipoRes {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.component.html',
  styleUrls: ['./create-booking.component.css'],
  providers: [MessageService],
})
export class CreateBookingComponent {
  private getAuthResponse(): AuthenticationResponse | null {
    const storedUser = localStorage.getItem('user');
    if (storedUser) {
      return JSON.parse(storedUser);
    }
    return null;
  }

  get id(): string {
    const authResponse = this.getAuthResponse();
    if (
      authResponse &&
      authResponse.data.customerId &&
      authResponse.data.customerId
    ) {
      return authResponse.data.customerId;
    }
    return '00';
  }

  typesRes: tipoRes[] = [
    { value: 'CENA', viewValue: 'CENA' },
    { value: 'ALMUERZO', viewValue: 'ALMUERZO' },
    { value: 'ONCES', viewValue: 'ONCES' },
    { value: 'CUMPLEAÑOS', viewValue: 'CUMPLEAÑOS' },
    { value: 'OCASION ESPECIAL', viewValue: 'OCASION ESPECIAL' },
    { value: 'OTRO', viewValue: 'OTRO' },
  ];

  hours: tipoRes[] = [
    { value: '03:00', viewValue: '03:00' },
    { value: '04:00', viewValue: '04:00' },
    { value: '05:00', viewValue: '05:00' },
    { value: '06:00', viewValue: '06:00' },
    { value: '07:00', viewValue: '07:00' },
    { value: '08:00', viewValue: '08:00' },
    { value: '09:00', viewValue: '09:00' },
    { value: '10:00', viewValue: '10:00' },
    { value: '11:00', viewValue: '11:00' },
    { value: '12:00', viewValue: '12:00' },
  ];

  formNewBooking!: FormGroup;
  dateReserva!: FormControl;
  tipoReserva!: FormControl;
  hourStart!: FormControl;
  hourFinish!: FormControl;
  quantityPersons!: FormControl;
  observations!: FormControl;
  stusReserva!: FormControl;
  idCustomer!: FormControl;

  dat: any;

  constructor(
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private reservaService: ReservaService
  ) {

    this.formNewBooking = this.formBuilder.group({
      dateReserva: new FormControl('', [Validators.required]),
      tipoReserva: new FormControl('', [Validators.required]),
      hourStart: new FormControl(),
      hourFinish: new FormControl(),
      quantityPersons: new FormControl('', [Validators.required]),
      observations: new FormControl('', [Validators.required]),
      idCustomer: new FormControl(this.id),
    });

  }

  saveReserva() {
    // Clonar los valores del formulario inicializado
    this.dat = { ...this.formNewBooking.value };

    // Formatear la fecha
    this.dat.dateReserva = this.datePipe.transform(
      this.dat.dateReserva,
      'yyyy-MM-dd'
    );

    //console.log(this.dat);

    // Actualizar el formulario con los valores formateados
    this.formNewBooking.patchValue({
      dateReserva: this.dat.dateReserva,
    });

    const dataForm = this.formNewBooking.value;
    this.reservaService.registerReserva(dataForm).subscribe({
      next: () => {
        this.messageService.add({
          severity: 'success',
          summary: 'Success',
          detail: `The booking was created successfully`,
        });
      },
      error: (err) => {
        console.log('fracaso');
      },
    });
  }
}
