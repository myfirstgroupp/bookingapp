import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { CustomerRegistrationRequest } from '../models/customer-registration-request';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  private customerURL = `${environment.api.baseURL}/${environment.api.customerURL}/auth/signup`;

  constructor(private http: HttpClient) {}

  registerCustomer(customer: CustomerRegistrationRequest) {
    const userJson = JSON.stringify(customer);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .post<number>(this.customerURL, userJson, { headers })
      .pipe(map((customer_id: number) => customer_id));
  }
}
