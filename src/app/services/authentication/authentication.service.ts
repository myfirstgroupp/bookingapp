import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthRequest } from 'src/app/models/auth.request';
import { AuthenticationResponse } from 'src/app/models/authentication-response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly authUrl = `${environment.api.baseURL}/${environment.api.authURL}`;
  currentUserData: BehaviorSubject<String> = new BehaviorSubject<String>('');

  constructor(private http: HttpClient) {}

  login(authRequest: AuthRequest): Observable<AuthenticationResponse> {
    return this.http.post<AuthenticationResponse>(this.authUrl, authRequest);
  }

  get userToken(): String {
    return this.currentUserData.value;
  }
}
