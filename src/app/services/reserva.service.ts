import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BookingCreateRequest } from '../models/booking-create-request';
import { Observable, catchError, map, throwError } from 'rxjs';
import { AuthenticationResponse } from '../models/authentication-response';

@Injectable({
  providedIn: 'root',
})
export class ReservaService {
  token?: string | null = null;

  private reservaURL = `${environment.api.baseURL}${environment.api.reservaURL}`;
  private getReservaURL = `${environment.api.baseURL}${environment.api.getReservasURL}`;

  constructor(private http: HttpClient) {}

  registerReserva(reserva: BookingCreateRequest) {
    const storedUser = localStorage.getItem('user');

    if (storedUser) {
      const authResponse: AuthenticationResponse = JSON.parse(storedUser);
      //TOKEN EXTRACTION FROM API RESPONSE BACKEND
      this.token = authResponse.jwt;
    }

    if (!this.token) {
      console.error('NO TOKEN FOUND');
    }

    const userJson = JSON.stringify(reserva);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json',
      Authorization: `Bearer ${this.token}`,
    });
    return this.http.post(this.reservaURL, userJson, { headers });
  }

  getReservas(id: string): Observable<BookingCreateRequest[]> {
    return this.http
      .get<{ data: BookingCreateRequest[] }>(`${this.getReservaURL + id}`)
      .pipe(
        map((response) => response.data) // Mapea la respuesta para extraer el array de datos
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('Se ha producio un error ', error.error);
    } else {
      console.error(
        'Backend retornó el código de estado ',
        error.status,
        error.error
      );
    }
    return throwError(
      () => new Error('Algo falló. Por favor intente nuevamente.')
    );
  }
}
