import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { AuthRequest } from 'src/app/models/auth.request';
import { AuthService } from 'src/app/services/authentication/authentication.service';

//primeNG
import { Message, MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService],
})
export class LoginComponent implements OnInit {
  messages!: Message[];
  loginForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private messageService: MessageService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: ['', Validators.required],
    });
  }

  login() {
    if (this.loginForm.valid) {
      const authRequest: AuthRequest = this.loginForm.value;
      this.authService.login(authRequest).subscribe({
        next: (authenticationResponse) => {
          localStorage.setItem('user', JSON.stringify(authenticationResponse));
          this.router.navigate(['profile']);
        },
        error: (err) => {
          if (err.error.error === 'user not registered!') {
            this.messageService.add({
              severity: 'warn',
              summary: 'Warning',
              detail: ` User ${authRequest.email} not found`,
            });
          }
          if (err.error.error === 'Authentication failed!') {
            this.messageService.add({
              severity: 'error',
              summary: 'Error',
              detail: ` Wrong Password`,
            });
          }
        },
      });
    }
  }
}
