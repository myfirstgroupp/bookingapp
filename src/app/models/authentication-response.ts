import { CustomerDTO } from "./customer-dto";

export interface AuthenticationResponse {
 jwt?: string;
 data: CustomerDTO;
}
