export interface CustomerRegistrationRequest {
    tipoDocument: string;
    document: string;
    firstName: string;
    lastName: string;
    phone: string;
    birthdate: Date;
    email: string;
    password: string;
}
