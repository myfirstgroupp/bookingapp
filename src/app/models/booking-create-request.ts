export interface BookingCreateRequest {
  dateReserva: Date;
  tipoReserva: string;
  hourStart: string;
  hourFinish: string;
  quantityPersons: number;
  observations: string;
  stusReserva: string;
  idCustomer: number;
}
