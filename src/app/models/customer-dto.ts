export interface CustomerDTO {
  customerId: string;
  tipoDocument: string;
  document: string;
  firstName: string;
  lastName: string;
  phone: string;
  birthdate: Date;
  email: string;
  rol: number;
}
