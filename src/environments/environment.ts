export const environment = {
  api: {
    baseURL: 'http://localhost:8080',
    authURL: 'api/v1/auth/signin',
    customerURL: 'api/v1',
    reservaURL: '/api/v1/reservas/create',
    getReservasURL: '/api/v1/reservas/'
  },
};
