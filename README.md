
## <img width="10%" src="https://www.svgrepo.com/show/140658/calendar.svg" style="background-color:#c9d1d9"> Bookings App

Booking App is an web application for secure and accurate booking managment.
Tecnologies:
Angular version 15.2.10.
Typescript
Spring Boot
Java
MySql
JWT

The main funcionatilies are described below:
- **Customer**
  - Register
  - Login
  - Update
  - Create reservation
  - Update reservation
- **Admin**
  - Login
  - Register user
  - Update user
  - Delete user
  - Create admin
    - Update admin
    - Delete admin
  - Confirm booking
  - Manage booking


**Booking App works with Rest API:** [Backend Spring Boot](https://github.com/santiagoserpa55/back-reservations) 
## Technical Specifications

# BookingApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.10.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
